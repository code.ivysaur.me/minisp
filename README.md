# minisp

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A lisp interpreter based on Peter Norvig's lisp.py.

Tags: PL

## See Also

- http://norvig.com/lispy.html "(How to Write a (Lisp) Interpreter (in Python))"
- http://norvig.com/lispy2.html "(An ((Even Better) Lisp) Interpreter (in Python))"


## Download

- [⬇️ minisp-r1.zip](dist-archive/minisp-r1.zip) *(1.90 KiB)*
